# Backup Manager

## Streams Platform Addon

A graphical user interface (GUI), allowing you to backup PyroCMS database and tables.

### Features
* Backup your database or
* Select the tables / streams you would like to create a backup of
* Easily restore database and tables

### Installation
* Add this module to your PyroCMS project as you would any other module.
* Install it using the PyroCMS or simply run the following command `php artisan module:install backup_manager`
* A new menu item will appear in your admin.

### Create a backup
* Click the menu item "Backup Manager".
* Then click the button "Create a dump".
* Enter the name of the backup you are to create. Select addon to backup or nothing to select the complete database.
* Press save and you are done.

### Restore a backup
* Click the menu item "Backup Manager".
* Click restore on the backup you would like to restore.
